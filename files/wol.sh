#!/bin/bash
#
# Send Wake on LAN magic packets to machines
#
# First argument: IP address of machine
# Second argument: MAC address of machine

IP_ADDRESS=$1
MAC_ADDRESS=$2

if [[ ! "$MAC_ADDRESS" =~ ^([a-fA-F0-9]{2}:){5}[a-fA-F0-9]{2}$ ]]; then
    echo "Invalid MAC provided"
    exit 2
fi

VLAN=$(echo "$IP_ADDRESS" | awk -F'.' '{print $2}')
if [ -z "$VLAN" ]; then
    echo "No proper IP address was provided"
    exit 3
else
    INTERFACE=$(ip -d -j link sh type vlan | jq -r --arg vlanid "$VLAN" '[.[] | select(.linkinfo.info_data.id == ($vlanid | tonumber))] | first | .ifname | select(. != null)')
fi

if [ -z "$INTERFACE" ]; then
    echo "No VLAN interface available to send WoL"
    exit 4
else
    etherwake -i "$INTERFACE" "$MAC_ADDRESS"
fi
